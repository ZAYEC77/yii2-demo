<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RadioAsset
 *
 * @author nestor
 */
namespace app\assets;

use yii\web\AssetBundle;

class RadioAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'css/skin/blue.monday/css/jplayer.blue.monday.css',
    ];
    public $js = [
        'js/vendor/jplayer/jquery.jplayer.min.js',
        'js/radio.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
